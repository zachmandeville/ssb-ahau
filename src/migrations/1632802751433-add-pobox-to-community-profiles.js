const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const FindAuthoredTribes = require('../lib/find-authored-tribes')

module.exports = {
  up (ssb, misc, cb) {
    const findAuthoredTribeIds = FindAuthoredTribes(ssb)

    function findAdminSubGroupByGroupId (groupId, cb) {
      ssb.tribes.findSubGroupLinks(groupId, (err, subGroupLinks) => {
        if (err) return cb(err)

        const admin = subGroupLinks.filter(link => link.admin)

        if (admin.length === 0) return cb(new Error('found group without an admin subgroup'))

        cb(null, { groupId, adminSubGroupId: admin[0].subGroupId })
      })
    }

    function getAdminSubGroupData ({ groupId, adminSubGroupId }, cb) {
      ssb.tribes.get(adminSubGroupId, (err, data) => {
        if (err) return cb(err)

        cb(null, { parentGroupId: groupId, adminSubGroup: data })
      })
    }

    function pullPoBox (root, cb) {
      const query = [{
        $filter: {
          dest: root,
          value: {
            content: {
              type: 'group/po-box',
              tangles: {
                poBox: { root }
              }
            }
          }
        }
      }]

      pull(
        ssb.backlinks.read({ query }),
        pull.map(m => m.value.content?.keys?.set?.poBoxId),
        pull.filter(Boolean), // (this will drop entries where were empty
        pull.take(1), // stop once we have the first match
        pull.collect((err, data) => {
          if (err) return cb(err)

          cb(null, data[0])
        })
      )
    }

    function findPoBoxIdBySubGroupInit ({ parentGroupId, adminSubGroup }, cb) {
      pullPoBox(adminSubGroup.root, (err, poBoxId) => {
        if (err) return cb(err)

        cb(null, { parentGroupId, poBoxId })
      })
    }

    function findProfilesByGroupId ({ parentGroupId, poBoxId }, cb) {
      const getProfile = (id, cb) => {
        ssb.get({ id, private: true }, (err, value) => {
          if (err) return cb(err)

          const { recps } = value.content

          cb(null, { id, poBoxId, recps })
        })
      }

      ssb.profile.findByGroupId(parentGroupId, { getProfile }, (err, profiles) => {
        if (err) return cb(err)

        cb(null, [...profiles.public, ...profiles.private])
      })
    }

    function updateProfileWithPoBoxId ({ id, poBoxId, recps }, cb) {
      const isPublicProfile = recps === undefined
      const updateProfile = isPublicProfile
        ? ssb.profile.community.public.update
        : ssb.profile.community.group.update

      updateProfile(id, { poBoxId, allowPublic: isPublicProfile }, cb)
    }

    findAuthoredTribeIds((err, tribeIds) => {
      if (err) return cb(err)

      pull(
        pull.values(tribeIds),
        paraMap(findAdminSubGroupByGroupId, 5), // use the groups we now have to find their linked admin subGroup
        paraMap(getAdminSubGroupData, 5), // use the admin subGroupId to get the group/init message for that subGroup
        paraMap(findPoBoxIdBySubGroupInit, 5), // use the groupInit message for that subGroup to get the poBoxId
        paraMap(findProfilesByGroupId, 5), // find all profiles linked to the parentGroup
        pull.flatten(),
        paraMap(updateProfileWithPoBoxId, 5), // update the profiles with the poBoxId!
        pull.collect(cb)
      )
    })
  }
}
