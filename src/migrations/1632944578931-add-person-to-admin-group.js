const pull = require('pull-stream')
const paraMap = require('pull-paramap')

const FindAuthoredTribes = require('../lib/find-authored-tribes')
const copyDetails = require('../lib/copy-profile-details')

/* context:
 * we have changed the group application process such that applicants create a copy of their
 * source profile in the kaitiaki subgroup
 *
 * in this migration, kaitiaki that created groups check for people in their group who do
 * not have an admin profile set up, and if they don't, makes them one
 */

module.exports = {
  up (ssb, misc, cb) {
    const findAuthoredTribeIds = FindAuthoredTribes(ssb)

    function findAdminSubGroupByGroupId (groupId, cb) {
      ssb.tribes.findSubGroupLinks(groupId, (err, subGroupLinks) => {
        if (err) return cb(err)

        const admin = subGroupLinks.filter(link => link.admin)
        if (admin.length === 0) return cb(new Error('found group without an admin subgroup'))

        cb(null, { groupId, adminSubGroupId: admin[0].subGroupId })
      })
    }

    function findPoBoxByGroupId (groupId, cb) {
      findAdminSubGroupByGroupId(groupId, (err, { adminSubGroupId }) => {
        if (err) return cb(err)

        ssb.tribes.poBox.get(adminSubGroupId, (err, poBox) => {
          if (err) return cb(err)

          cb(null, { groupId, poBoxId: poBox.poBoxId })
        })
      })
    }

    function findFeedProfileLinks (cb) {
      const query = [{
        $filter: {
          value: {
            content: {
              type: 'link/feed-profile',
              tangles: {
                link: { root: null, previous: null } // root ones only
              }
              // recps: handled below
            }
          }
        }
      }]

      pull(
        ssb.query.read({ query }),
        pull.filter(m => m.value.content.recps), // only private ones
        pull.collect(cb)
      )
    }

    function copyGroupToAdminProfile ({ groupProfileId, feedId, poBoxId }, cb) {
      ssb.profile.person.group.get(groupProfileId, (err, groupProfile) => {
        if (err) return cb(err)

        const details = {
          authors: { add: [feedId, '*'] },
          recps: [poBoxId, feedId]
        }
        copyDetails(groupProfile.states[0], details)

        ssb.profile.person.admin.create(details, (err, profileId) => {
          if (err) return cb(err)
          ssb.profile.link.create(profileId, { feedId }, cb)
        })
      })
    }

    // get the tribes we authored
    findAuthoredTribeIds((err, tribeIds) => {
      if (err) return cb(err)

      if (tribeIds.length === 0) return cb(null, 0)

      // load up all feed-profile links we can see
      findFeedProfileLinks((err, links) => {
        if (err) return cb(err)

        const addFeedsThatNeedAdmin = AddFeedsThatNeedAdmin(links)

        pull(
          pull.values(tribeIds),
          // load the poBox fro each tribes
          paraMap(findPoBoxByGroupId, 5),

          // attach feeds that need admin-profile to each tribe
          paraMap(addFeedsThatNeedAdmin, 5),

          paraMap(({ groupId, poBoxId, feeds }, cb) => {
            const input = Object.entries(feeds)
              .map(([feedId, groupProfileId]) => ({ feedId, groupProfileId, poBoxId }))

            pull(
              // create the admin-profiles for each feedId from their group-profile details, link them up
              pull.values(input),
              paraMap(copyGroupToAdminProfile, 4),
              pull.collect(cb)
            )
          }),
          pull.collect((err, data) => {
            if (err) return cb(err)

            const totalUpdates = data.reduce((acc, next) => acc + next.length, 0)
            cb(null, totalUpdates)
          })
        )
      })
    })
  }
}

function AddFeedsThatNeedAdmin (links) {
  return function ({ groupId, poBoxId }, cb) {
    pull(
      pull.values(links),
      pull.map(m => m.value.content),
      pull.filter(m => m.recps && (m.recps[0] === groupId || m.recps[0] === poBoxId)),
      pull.unique(m => m.child), // unique profileIds
      pull.collect((err, data) => {
        if (err) return cb(err)

        const feeds = data
          .reduce(
            (acc, link) => {
              if (!acc[link.parent]) acc[link.parent] = { group: [], admin: [] }
              if (link.recps[0] === groupId) acc[link.parent].group.push(link)
              if (link.recps[0] === poBoxId) acc[link.parent].admin.push(link)
              return acc
            },
            {
              // feedId: {
              //   group: [],
              //   admin: []
              // }
            }
          )

        // filter out the feeds that don't need an admin profile
        for (const feedId in feeds) {
          if (feeds[feedId].group.length && feeds[feedId].admin.length) {
            delete feeds[feedId]
          } // eslint-disable-line
          else {
            feeds[feedId] = feeds[feedId].group[0].child
          }
        }

        cb(null, {
          groupId,
          poBoxId,
          feeds // { feedId: groupProfileId } (only feeds than need an admin profile)
        })
      })
    )
  }
}
