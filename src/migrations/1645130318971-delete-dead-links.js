const pull = require('pull-stream')
const flatMap = require('pull-flatmap')
const merge = require('pull-merge')

module.exports = {
  up (ssb, misc, cb) {
    let count = 0

    pull(
      tombstonedProfileIdSource(ssb),

      // find all un-tombstoned links to those person(s)
      pull.asyncMap((profileId, cb) => mapToLinkInfo(ssb, profileId, cb)),
      flatMap(arr => arr),

      pull.asyncMap((linkInfo, cb) => {
        ssb.whakapapa.link.tombstone(linkInfo.id, linkInfo.details, (err, data) => {
          if (err) console.error(`tombstone link failed ${linkInfo.id}`)
          else count++

          cb(null, linkInfo.id)
        })
      }),

      pull.collect((err, vals) => {
        if (err) return cb(err)

        cb(null, count)
      })
    )
  }
}

function tombstonedProfileIdSource (ssb) {
  // find all profile/person or profile/person/admin that have been tombstoned
  const query = (type) => ({
    query: [{
      $filter: {
        value: {
          content: {
            type,
            tombstone: { $is: 'object' }
          }
        }
      }
    }]
  })

  return pull(
    merge(
      ssb.query.read(query('profile/person')),
      ssb.query.read(query('profile/person/admin')),
      (a, b) => 1
    ),
    pull.filter(msg => msg.value.content?.tangles?.profile?.root),
    pull.map(msg => msg.value.content.tangles.profile.root)
  )
}

function mapToLinkInfo (ssb, profileId, cb) {
  const query = [{
    $filter: { dest: profileId }
  }]

  return pull(
    ssb.backlinks.read({ query }),
    pull.filter(msg => {
      const { type, parent, child } = msg.value.content
      return (
        type.startsWith('link/') &&
        (parent === profileId || child === profileId)
      )
    }),

    // only interested in comments that aren't already tombstoned
    pull.asyncMap((msg, cb) => {
      ssb.whakapapa.link.get(msg.key, (err, link) => {
        if (err) cb(null, null)
        else cb(null, link)
      })
    }),
    pull.filter(link => link && link.states.some(state => state.tombstone === null)),

    pull.map(link => {
      return {
        id: link.key,
        details: link.recps ? {} : { allowPublic: true }
      }
    }),
    pull.collect(cb)
  )
}
