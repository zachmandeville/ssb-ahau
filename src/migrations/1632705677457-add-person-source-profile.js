const LoadPersonalGroup = require('@ssb-graphql/main/src/ssb/load-context/load-personal-group')
const copyDetails = require('../lib/copy-profile-details')

// context: the default for profiles in groups was updated to be called "profile.person.group"
// It has the same type, but now exlcudes contact details
// Unfortunately your personal profiles used this type as well, but we want contact details on that one!
// The solution is to migrate older personal profiles that are found over to the new type `profile/person/source`
//
// NOTE: this migration drops contact details, but this was deemed acceptable

module.exports = {
  up (ssb, _, cb) {
    const loadPersonalGroupId = LoadPersonalGroup(ssb)

    loadPersonalGroupId((err, groupId) => {
      if (err) return cb(err)

      ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
        if (err) return cb(err)

        /* find an existing profile */
        const personalProfiles = filterProfiles(profiles.private, 'person/source', groupId)
        if (personalProfiles.length) {
          if (personalProfiles.length > 1) console.warn('ssb-graphql/main found multiple personal profiles, using first one')
          return cb(null)
        }

        /* create a new profile */
        const details = {
          authors: {
            add: [ssb.id]
          },
          recps: [groupId]
        }

        /* check for LEGACY personal profile */
        const legacyProfiles = filterProfiles(profiles.private, 'person', groupId)
        if (legacyProfiles.length) {
          copyDetails(legacyProfiles[0].states[0], details)
        }

        ssb.profile.person.source.create(details, (err, profileId) => {
          if (err) return cb(err)

          ssb.profile.link.create(profileId, {}, (err, link) => {
            if (err) return cb(err)

            if (legacyProfiles.length === 0) return cb(null, profileId)

            ssb.profile.person.group.tombstone(legacyProfiles[0].key, {}, (err) => {
              if (err) return cb(err)
              cb(null, profileId)
            })
          })
        })
      })
    })
  }
}

function filterProfiles (profiles, type, groupId) {
  return profiles.filter(p => (
    p.states[0].type === type &&
    p.recps.length === 1 &&
    p.recps[0] === groupId
  ))
}
