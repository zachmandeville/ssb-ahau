const pull = require('pull-stream')
const { promisify: p } = require('util')
const Application = require('ssb-tribes/method/deprecated/application.js')

module.exports = {
  up (ssb, misc, cb) {
    let source

    /* load your personal/source profile */
    LoadPersonalGroup(ssb)((err, groupId) => {
      if (err) return cb(err)
      LoadSourceProfile(ssb)(groupId, (err, profile) => {
        if (err) return cb(err)
        source = profile

        migrateApplications()
      })
    })

    function migrateApplications () {
      try {
        const application = Application(ssb)

        application.list({ accepted: null, get: application.get }, (err, applications) => {
          if (err) return cb(err)

          pull(
            pull.values(applications),
            pull.filter(a => a.applicantId === ssb.id),
            pull.asyncMap((a, cb) => {
              const { groupId, answers, history } = a
              const comment = history.find(el => el.author === ssb.id && el.type === 'comment')?.body

              createRegistration(ssb, source, groupId, { answers, comment })
                .then(reg => {
                  application.tombstone(a.id, { reason: 'migrated to registrion/group' }, cb)
                })
                .catch(cb)
            }),
            pull.collect((err, results) => {
              if (err) return cb(err)
              cb(null, results.length)
            })
          )
        })
      } catch (err) {
        console.log('migrateApplications failed:', err)
        cb(null)
      }
    }
  }
}
// NOTE the follow function have been copied from mentioned sources, and modified *slightly*
// to just grab what we need
// We assume for example that personal/source profile already exists and doesn't need creating
// We've left async functions, but used them with callback style .then.catch to minimise edits

/* copied from @ssb-graphql/tribes */
async function createRegistration (ssb, source, groupId, { answers, comment }) {
  /* find group poBoxId */
  const profiles = await p(ssb.profile.findByGroupId)(groupId)
  const poBoxId = getPOBoxFromProfiles(profiles)
  if (!poBoxId) {
    console.log(`unable to find poBoxId for group ${groupId}`)
    return
  }
  // WARNING!!! this edge case is bad

  const recps = [poBoxId, ssb.id]

  /* publish a profile/person/admin for the kaitiaki of the group */
  const details = {
    ...copyDetails(source.states[0]),
    authors: { add: [ssb.id, '*'] }, // allow me + kaitiaki to edit
    recps
  }

  const adminProfileId = await p(ssb.profile.person.admin.create)(details)
  await p(ssb.profile.link.create)(adminProfileId) // links it to ourself

  // if (answers) answers = answers.map(d => ({ q: d.question, a: d.answer }))

  // returns the id of the application
  return p(ssb.registration.tribe.create)(groupId, { answers, comment, profileId: adminProfileId, recps })
}

function copyDetails (state) {
  const details = {}
  let value
  for (const field in state) {
    if (field === 'key') continue
    if (field === 'type') continue
    if (field === 'authors') continue

    value = state[field]
    if (isEmpty(value)) continue

    if (field === 'altNames') details[field] = { add: value } // simple-set field
    else details[field] = value // overwrite fields
  }
  return details
}

function isEmpty (value) {
  if (value === null) return true
  // NOTE should never be undefined
  if (typeof value === 'string' && value.length === 0) return true
  if (Array.isArray(value) && value.length === 0) return true

  return false
}

function getPOBoxFromProfiles (profiles) {
  if (profiles.public.length === 0) return
  if (profiles.public.length > 1) console.warn('found more than one public community profile!')

  return profiles.public[0].states[0].poBoxId
}

/* copied from @ssb-graphql/main */
const PERSONAL_TYPE = '__personal__'
function LoadPersonalGroup (ssb) {
  return function loadPersonalGroup (cb) {
    if (cb === undefined) return p(loadPersonalGroup)()

    ssb.tribes.findByFeedId(ssb.id, (err, groups) => {
      if (err) return cb(err)

      const personalGroups = groups.filter(group => {
        return group.states[0].state.name === PERSONAL_TYPE
      })
      if (personalGroups.length) {
        if (personalGroups.length > 1) console.warn('ssb-graphql/main found multiple personal groups, using first one')
        return cb(null, personalGroups[0].groupId)
      }

      cb(new Error('could not find personal group'))
    })
  }
}

function LoadSourceProfile (ssb) {
  return function loadSourceProfile (groupId, cb) {
    if (cb === undefined) return p(loadSourceProfile)(groupId)

    ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
      if (err) return cb(err)

      /* find an existing profile */
      const personalProfiles = filterProfiles(profiles.private, 'person/source', groupId)
      if (personalProfiles.length) {
        if (personalProfiles.length > 1) console.warn('ssb-graphql/main found multiple personal profiles, using first one')
        return cb(null, personalProfiles[0])
      }

      return cb(new Error('could not find personal source profile'))
    })
  }
}

function filterProfiles (profiles, type, groupId) {
  return profiles.filter(p => (
    p.states[0].type === type &&
    p.recps.length === 1 &&
    p.recps[0] === groupId
  ))
}
