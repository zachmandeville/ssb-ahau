const gql = require('graphql-tag')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { promisify: p } = require('util')
const omit = require('lodash.omit')
const fixInput = require('@ssb-graphql/profile/src/lib/fix-input')

const FindAdminSubGroupByGroupId = require('../lib/find-admin-subgroup-by-group-id')
const copyDetails = require('../lib/copy-profile-details')

module.exports = (ssb) => {
  const resolvers = Resolvers(ssb)

  return {
    typeDefs,
    resolvers,
    gettersWithCache: {}
  }
}

const typeDefs = gql`
  type InitGroupDetails {
    groupId: String
    adminSubGroupId: String
    poBoxId: String
  }

  extend type Mutation {
    """
    Create a new group and profiles, and creates a kaitiaki-only subgroup and profiles
    """
    initGroup(communityProfile: CommunityProfileInput): InitGroupDetails!

    """
    Adds a new kaitiaki to an admin subgroup using the parent groups id
    """
    addAdminsToGroup(groupId: String! adminIds: [String!]!): String!
  }

  extend type TribeFaces {
    admin: AdminTribeFace
  }

  type AdminTribeFace {
    id: ID # groupId of the admin-only subtribe
    poBoxId: String
  }
`

function Resolvers (ssb) {
  const findAdminSubGroupByGroupId = FindAdminSubGroupByGroupId(ssb)

  async function initGroup (input, context) {
    // get the source profile (personal profile)
    const source = await p(ssb.profile.person.source.get)(context.personal.profileId)

    const { groupId } = await p(ssb.tribes.create)({})
    // make a profile for you in this group

    let details = {
      authors: {
        add: [ssb.id]
      },
      recps: [groupId]
    }
    copyDetails(source.states[0], details, { isGroup: true })
    let profileId = await p(ssb.profile.person.group.create)(details)
    await p(ssb.profile.link.create)(profileId)

    // create an admin subgroup
    const { poBoxId, groupId: adminSubGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

    details = {
      authors: {
        add: [ssb.id, '*']
      },
      recps: [poBoxId, ssb.id]
    }
    copyDetails(source.states[0], details)
    profileId = await p(ssb.profile.person.admin.create)(details)
    await p(ssb.profile.link.create)(profileId)

    input.poBoxId = poBoxId

    await initGroupProfiles(groupId, input)

    return { groupId, adminSubGroupId, poBoxId }
  }

  const privateOnlyFields = ['allowWhakapapaViews', 'allowStories', 'allowPersonsList']
  const publicOnlyFields = ['joiningQuestions', 'customFields']

  async function initGroupProfiles (groupId, input) {
    // auto set authors to the creator
    const details = {
      ...fixInput.community(input), // includes poBoxId
      authors: {
        add: [ssb.id]
      }
    }

    // // create the groups public profile
    const groupPublicProfileId = await p(ssb.profile.community.public.create)({ ...omit(details, privateOnlyFields), allowPublic: true })
    await p(ssb.profile.link.create)(groupPublicProfileId, { groupId, allowPublic: true })

    // create the groups private profile
    const groupProfileId = await p(ssb.profile.community.group.create)({ ...omit(details, publicOnlyFields), recps: [groupId] })
    await p(ssb.profile.link.create)(groupProfileId, { groupId })

    return groupId
  }

  const getAdminSubGroupByGroup = (parentGroup, cb) => {
    ssb.tribes.findSubGroupLinks(parentGroup.id, (err, links) => {
      if (err) return cb(err)

      pull(
        pull.values(links),
        pull.filter(link => link.admin),
        pull.collect((err, adminSubGroups) => {
          if (err) return cb(err)

          if (!adminSubGroups || !adminSubGroups.length) return cb(null, null)
          if (adminSubGroups.length > 1) console.warn('ssb-ahau found multiple admin subgroups linked to a group, choosing the first one')
          cb(null, { id: adminSubGroups[0].subGroupId, poBoxId: parentGroup.public[0].poBoxId })
        })
      )
    })
  }

  function findProfilesByGroupId (groupId, cb) {
    const getProfile = (id, cb) => {
      ssb.get({ id, private: true }, (err, value) => {
        if (err) return cb(err)

        const { recps } = value.content

        cb(null, { id, recps })
      })
    }

    ssb.profile.findByGroupId(groupId, { getProfile }, (err, profiles) => {
      if (err) return cb(err)

      cb(null, [...profiles.public, ...profiles.private])
    })
  }

  function updateCommunityGroupProfiles (groupId, adminIds, cb) {
    findProfilesByGroupId(groupId, (err, profiles) => {
      if (err) return cb(err)

      pull(
        pull.values(profiles),
        paraMap(
          ({ id, recps }, cb) => {
            const isPublicProfile = recps === undefined
            const updateProfile = isPublicProfile
              ? ssb.profile.community.public.update
              : ssb.profile.community.group.update

            updateProfile(id, { authors: { add: adminIds }, allowPublic: isPublicProfile }, cb)
          },
          5
        ),
        pull.collect(cb)
      )
    })
  }

  async function addAdminsToGroup (groupId, adminIds) {
    // TODO: see if they are already a member first?
    const { adminSubGroupId } = await p(findAdminSubGroupByGroupId)(groupId)
    await p(ssb.tribes.invite)(adminSubGroupId, adminIds, {})
    await p(updateCommunityGroupProfiles)(groupId, adminIds)

    return groupId
  }

  const resolvers = {
    Mutation: {
      initGroup: (_, { communityProfile }, context) => initGroup(communityProfile, context),
      addAdminsToGroup: (_, { groupId, adminIds }) => addAdminsToGroup(groupId, adminIds)
    },
    TribeFaces: {
      admin: (parentGroup) => p(getAdminSubGroupByGroup)(parentGroup)
    },
    AdminTribeFace: {
      poBoxId: (adminGroup) => adminGroup.poBoxId
    }
  }

  return resolvers
}
