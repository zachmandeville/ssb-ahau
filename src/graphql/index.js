const ahauServer = require('ahau-graphql-server')
const { promisify } = require('util')

/* Hack */
let NODE_ENV = process.env.NODE_ENV
if (NODE_ENV === 'test') NODE_ENV = 'development'
const { ahau: env } = require('ahau-env')(NODE_ENV)
// need ahau-env 'development' variables (there are no testing ones!)
// but don't want NODE_ENV = 'development' globally as this turns on logging

const Main = require('@ssb-graphql/main')
const Tribes = require('@ssb-graphql/tribes')
const Profile = require('@ssb-graphql/profile')
const Artefact = require('@ssb-graphql/artefact')
const Story = require('@ssb-graphql/story')
const Whakapapa = require('@ssb-graphql/whakapapa')
const Invite = require('@ssb-graphql/invite')
const Submissions = require('@ssb-graphql/submissions')
const Settings = require('@ssb-graphql/settings')

const Author = require('./author')
const GroupInit = require('./group-init')
const Backup = require('./backup')
const Stats = require('./stats')

module.exports = async function graphqlServer (ssb) {
  const PORT = (ssb.config.graphql && ssb.config.graphql.port) || env.graphql.port

  const main = Main(ssb)
  const profile = Profile(ssb)
  const tribes = Tribes(ssb, { ...profile.gettersWithCache })
  const story = Story(ssb)
  const artefact = Artefact(ssb)
  const whakapapa = Whakapapa(ssb, {
    ...profile.gettersWithCache,
    ...story.gettersWithCache,
    ...artefact.gettersWithCache
  })
  const submissions = Submissions(ssb, { ...profile.gettersWithCache })
  const settings = Settings(ssb)
  const author = Author(ssb, {
    ...profile.gettersWithCache
  })
  const groupInit = GroupInit(ssb)

  const invite = Invite(ssb)
  const backup = Backup(ssb)
  const stats = Stats(ssb)

  let context = {}
  if (ssb.config.graphql && ssb.config.graphql.loadContext !== false) {
    context = await promisify(main.loadContext)().catch(err => err)
  }
  if (context instanceof Error) throw context

  const httpServer = await ahauServer({
    schemas: [
      main,
      tribes,
      profile,
      artefact,
      story,
      whakapapa,
      submissions,
      settings,
      author,
      groupInit,
      invite,
      stats,
      backup
    ],
    context,
    port: PORT
  }).catch(err => err)

  if (httpServer instanceof Error) throw httpServer

  ssb.close.hook((close, args) => {
    httpServer.close()
    close(...args)
  })

  /*
    cordova-bridge is used in Cordova, DO NOT REMOVE!
    The if statement is uncommented by patch-package in the mobile platform
  */
  // if (process.env.PLATFORM === 'cordova') {
  //   require('cordova-bridge').channel.post(
  //     'initialized',
  //     JSON.stringify({ started: true })
  //   )
  // }

  return httpServer
}
