module.exports = function copyDetails (source, target, opts = {}) {
  let value
  for (const field in source) {
    if (field === 'key') continue
    if (field === 'type') continue
    if (field === 'authors') continue
    if (field === 'customFields') continue

    if (opts.isGroup) {
      if (field === 'address') continue
      if (field === 'email') continue
      if (field === 'phone') continue
    }

    value = source[field]
    if (isEmpty(value)) continue

    if (field === 'altNames') target[field] = { add: value } // simple-set field
    else target[field] = value // overwrite fields
  }

  return target
}

function isEmpty (value) {
  if (value === null) return true
  // NOTE should never be undefined
  if (typeof value === 'string' && value.length === 0) return true
  if (Array.isArray(value) && value.length === 0) return true

  return false
}
