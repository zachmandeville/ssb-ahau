const { generateMigration } = require('./migration-helpers')

const myArgs = process.argv.slice(2)
const filename = (myArgs.length === 0) ? 'untitled' : myArgs[0].replace(/\s/g, '-')

generateMigration(filename)
