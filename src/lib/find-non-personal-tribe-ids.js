module.exports = function FindNonPersonalTribeIds (ssb) {
  return function findNonPersonalTribeIds (cb) {
    ssb.tribes.findByFeedId(ssb.id, (err, personalGroups) => {
      if (err) return cb(err)

      personalGroups = new Set(personalGroups.map(g => g.groupId))

      ssb.tribes.list((err, tribeIds) => {
        if (err) return cb(err)

        const nonPersonalTribes = tribeIds.filter(groupId => !personalGroups.has(groupId))

        cb(null, nonPersonalTribes)
      })
    })
  }
}
