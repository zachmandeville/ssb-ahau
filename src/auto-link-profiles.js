const pull = require('pull-stream')

const CanEdit = require('./lib/can-edit')

function getSubType (recps) {
  if (recps[0].endsWith('cloaked')) return 'group'
  if (recps[0].startsWith('ssb:identity/po-box')) return 'admin'
}

module.exports = function autoLinkProfiles (ssb) {
  /* Context: sometimes we are in a group and someone else has created a profile and linked it to us
   * we generally want to have our own links to profiles we control
   *
   * Listen to other people linking us to profiles
   * - IF we already have a profile
   * -  THEN stop
   * -  ELSE
   *    - IF the link is to a profile we can author
   *    - THEN copy that link
   */

  const canEdit = CanEdit(ssb)

  /* state */
  const groupsWithProfile = {}

  // Listen to other people linking us to profiles
  const query = [{
    $filter: {
      dest: ssb.id,
      value: {
        content: {
          type: 'link/feed-profile',
          parent: ssb.id,
          tangles: {
            link: { root: null }
          }
        }
      }
    }
  }]

  pull(
    ssb.backlinks.read({ query, live: true }),
    pull.filter(m => !m.sync),
    pull.filter(m => m.value.author !== ssb.id && m.value.content.recps),

    // only keep links for groups we dont have a profile in
    pull.asyncMap((m, cb) => {
      hasProfileInGroup(m.value.content.recps[0], (err, hasProfile) => {
        if (err) return cb(null, null)

        cb(null, hasProfile ? null : m)
      })
    }),
    pull.filter(Boolean),

    // get the profiles that we can author
    pull.asyncMap((m, cb) => {
      const subType = getSubType(m.value.content.recps)
      if (!subType) return cb(null, null)

      ssb.profile.person[subType].get(m.value.content.child, (err, profile) => {
        if (err) return cb(null, null)

        canEdit(ssb.id, profile.states[0].authors, (err, can) => {
          if (err) return cb(null, null)

          cb(null, can ? profile : null)
        })
      })
    }),
    pull.filter(Boolean),

    // link the remainding profiles to our feedId
    pull.asyncMap((profile, cb) => ssb.profile.link.create(profile.key, cb)),
    pull.drain(
      link => { groupsWithProfile[link.recps[0]] = true },
      err => { if (err) console.error(err) }
    )
  )

  function hasProfileInGroup (groupId, cb) {
    if (groupsWithProfile[groupId]) return cb(null, true)

    ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
      if (err) return cb(err)

      const hasProfile = profiles.private
        .some(profile => profile.recps[0] === groupId)

      // if we already have a profile in the group
      cb(null, hasProfile)
    })
  }
}
