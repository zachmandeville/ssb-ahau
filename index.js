const path = require('path')
const pull = require('pull-stream')
const Obz = require('obz')
const Migrate = require('ssb-migrate')

// Note this is to ensure that migrations are all bundled
const migrations = {
  '1632705677457-add-person-source-profile.js': require('./src/migrations/1632705677457-add-person-source-profile'),
  '1632707381389-kaitaki-only-subgroups.js': require('./src/migrations/1632707381389-kaitaki-only-subgroups'),
  '1632802751433-add-pobox-to-community-profiles.js': require('./src/migrations/1632802751433-add-pobox-to-community-profiles'),
  '1632944578931-add-person-to-admin-group.js': require('./src/migrations/1632944578931-add-person-to-admin-group'),
  '1633378112058-move-applications-to-registrations.js': require('./src/migrations/1633378112058-move-applications-to-registrations'),
  '1645130318971-delete-dead-links.js': require('./src/migrations/1645130318971-delete-dead-links'),
  '1659581313702-add-admins-to-groups.js': require('./src/migrations/1659581313702-add-admins-to-groups.js')
}

const graphqlServer = require('./src/graphql')
const autoLinkProfiles = require('./src/auto-link-profiles')

module.exports = {
  name: 'ahau',
  version: '1.0.0',
  manifest: {
    onReady: 'async'
  },
  init: function (ssb, config) {
    if (shouldLog()) {
      logPublish(ssb)
      logReplication(ssb)
    }

    const isReady = Obz()

    lanConn(ssb)
    autoLinkProfiles(ssb)

    // delay ensures all ssb-plugins have been installed
    setImmediate(() => {
      // NOTE: watch whether this is too slow...
      runMigrations(ssb, err => {
        if (err) {
          ssb.close()
          throw err
        }

        graphqlServer(ssb)
          .catch(err => { throw err })
          .then(graphql => isReady.set(graphql))
      })
    })

    return {
      onReady (cb) {
        isReady.once(val => cb(null))
      }
    }
  }
}

function shouldLog () {
  if (process.env.AHAU_LOGGING === 'true') return true
  if (
    process.env.NODE_ENV === 'development' &&
    process.env.AHAU_LOGGING !== 'false'
  ) return true

  return false
}

function logPublish (ssb) {
  pull(
    ssb.createUserStream({ id: ssb.id, live: true, old: false, private: true, meta: true }),
    pull.drain(m => {
      console.log('')
      console.log(m.value.sequence, m.key)
      console.log(JSON.stringify(m.value.content, null, 2))
      console.log('')
    })
  )
}

function logReplication (ssb) {
  pull(
    ssb.createLogStream({ live: true, old: false }),
    pull.filter(m => m.value.author !== ssb.id),
    pull.drain(m => {
      console.log(`replicated ${m.value.author}, seq: ${m.value.sequence}`)
    })
  )
}

function lanConn (ssb) {
  pull(
    ssb.lan.discoveredPeers(),
    pull.filter(p => p.verified),
    pull.drain(({ address, verified }) => {
      ssb.conn.connect(address, (err, peer) => {
        if (err) return console.error(err)
        if (!peer) return

        console.log('connected to local peer:', peer.id)
        // replicate their first 100 messages so we can see their name / avatar
        // pull(
        //   peer.createHistoryStream({ id: peer.id, keys: false, limit: 100 }),
        //   ssb.createWriteStream()
        // )
      })
    })
  )
}

function runMigrations (ssb, cb) {
  if (process.env.NODE_ENV === 'test') return cb(null)
  if (process.env.NODE_ENV === 'development') return cb(null)

  const migrationsPath = path.join(__dirname, 'src/migrations')
  new Migrate(ssb, migrationsPath, { verbose: true, migrations, resetIfCorrupt: true })
    .run(cb)
}
