const test = require('tape')
const path = require('path')

const os = require('os')
const fs = require('fs')
const TestBot = require('./test-bot')

test('delete ahau', async t => {
  // start the server
  const { ssb, apollo } = await TestBot({
    name: 'ahau-test',
    path: path.join(os.homedir(), '.ahau-test')
  })

  const platform = os.platform()

  t.pass('starts server')

  const result = await apollo.mutate({
    mutation: `
      mutation {
        deleteAhau
      }
    `
  }).catch(err => ({ errors: err }))

  t.error(result.errors, 'deletes ahau')
  if (result.errors) console.log(result.errors)

  t.true(result.data.deleteAhau, 'returns true for delete ahau mutation')

  if (platform === 'darwin' || platform === 'linux') { // Linux + Mac
    // darwin should delete these files fully
    t.false(
      fs.existsSync(ssb.config.path),
      `${platform} fully deleted ${ssb.config.path}`
    )
  } else if (platform === 'win32') {
    t.true(
      fs.existsSync(ssb.config.path),
      `${platform} partially deleted ${ssb.config.path} folder contents`
    )

    t.false(
      fs.existsSync(path.join(ssb.config.path, 'flume', 'log.offset')),
      'log.offset was deleted!'
    )
  } else {
    t.fail(`missing test for platform '${platform}'. Please add tests for this platform to test/delete-ahau.test`)
  }

  ssb.close()
  t.end()
})
