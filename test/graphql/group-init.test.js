const test = require('tape')
const { isCloakedMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const FindAdminSubGroupByGroupId = require('../../src/lib/find-admin-subgroup-by-group-id')
const Server = require('../test-bot')

function InitGroup (apollo, t) {
  return async function (communityProfile) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($communityProfile: CommunityProfileInput) {
          initGroup(communityProfile: $communityProfile) {
            groupId
          }
        }
      `,
      variables: {
        communityProfile
      }
    })

    t.error(res.errors, 'init group')

    return res.data.initGroup.groupId
  }
}

test('initGroup', async t => {
  const { apollo, ssb } = await Server()
  const initGroup = InitGroup(apollo, t)

  const handleErr = (err) => {
    // console.log(err) // JSON.stringify(err, null, 2))
    // console.log(JSON.stringify(err, null, 2))
    return { errors: [err] }
  }

  // ensure there are private details on your personal profile should be copied over to
  // the admin profile created for you in the admin-only subgroup
  const myProfiles = await p(ssb.profile.findByFeedId)(ssb.id, {})
  const personalProfileId = myProfiles.private[0].key
  await p(ssb.profile.person.source.update)(personalProfileId, { phone: '12345' })

  const customFields = [
    {
      key: Date.now().toString(), // timestamp
      type: 'text',
      label: 'Hometown',
      order: 1,
      required: false,
      visibleBy: 'members'
    }
  ]

  const communityProfile = {
    preferredName: 'Plant Collectors',
    description: 'For those who collect plants and want to share their collection',

    address: '123 Daisy Street',
    city: 'Hamilton',
    country: 'NZ',
    postCode: '3204',
    phone: '08 821 1128',
    email: 'plants@daisy.co.nz',

    joiningQuestions: [{ type: 'input', label: 'Do you collect plants?' }],
    customFields,

    // settings
    allowWhakapapaViews: true,
    allowStories: false,
    allowPersonsList: true
  }

  const groupId = await initGroup(communityProfile)
    .catch(handleErr)

  t.true(isCloakedMsgId(groupId), 'returns the groupId')

  const res = await apollo.query({
    query: `
      query($id: String!) {
        tribe(id: $id) {
          id
          public {
            preferredName
            poBoxId
            customFields {
              key
              type
              label
              required
              order
              visibleBy
            }
          }
          private {
            preferredName
            poBoxId
            allowWhakapapaViews
            allowStories
            allowPersonsList
            customFields {
              key
            }
          }
          admin {
            id
            poBoxId
          }
        }
      }
    `,
    variables: {
      id: groupId
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'tribe query')

  const tribe = res.data.tribe

  t.true(tribe.public.length === 1, 'one public profile for the group')
  t.true(tribe.private.length === 1, 'two private profiles for the group, one is the subgroups one')
  t.true(tribe.public[0].poBoxId === tribe.private[0].poBoxId, 'both profiles have the same poBoxId')
  t.true(typeof tribe.public[0].poBoxId === 'string', 'returns string for poBoxId')
  t.true(isCloakedMsgId(tribe.admin.id), 'returns the groupId for the admin subgroup')

  t.deepEqual(
    tribe.private[0],
    {
      preferredName: 'Plant Collectors',
      poBoxId: tribe.private[0].poBoxId, // hack
      // settings
      allowWhakapapaViews: true,
      allowStories: false,
      allowPersonsList: true,
      customFields: null // because customFields arent on the private profile
    },
    'returns correct fields on the private community profile'
  )

  t.deepEqual(
    tribe.public[0],
    {
      preferredName: 'Plant Collectors',
      poBoxId: tribe.public[0].poBoxId, // hack
      // settings
      customFields
    },
    'returns correct fields on the public community profile'
  )

  const updatedPersonalProfiles = await p(ssb.profile.findByFeedId)(ssb.id, {})
  const kaitiakiProfile = updatedPersonalProfiles.private[2]

  t.deepEqual(
    {
      recps: kaitiakiProfile.recps,
      phone: kaitiakiProfile.states[0].phone
    },
    {
      recps: [tribe.admin.poBoxId, ssb.id],
      phone: '12345'
    },
    'contact details were copied from your personal profile'
  )

  ssb.close()
  t.end()
})

test('multiple admin group kaitiaki', async t => {
  // ////
  // Setup users
  // ////
  const kaitiaki = await Server()
  const initGroup = InitGroup(kaitiaki.apollo, t)

  const findAdminSubGroupByGroupId = FindAdminSubGroupByGroupId(kaitiaki.ssb)
  const member = await Server()

  // ////
  // the kaitiaki initialises the group
  // ////
  const communityProfile = {
    preferredName: 'Plant Collectors',
    description: 'For those who collect plants and want to share their collection',

    address: '123 Daisy Street',
    city: 'Hamilton',
    country: 'NZ',
    postCode: '3204',
    phone: '08 821 1128',
    email: 'plants@daisy.co.nz',

    joiningQuestions: [{ type: 'input', label: 'Do you collect plants?' }]
  }

  const groupId = await initGroup(communityProfile)

  // ////
  //  the kaitiaki sees the community profiles in the group, the member is not an author
  // ////
  let profiles = await p(kaitiaki.ssb.profile.findByGroupId)(groupId)
  t.equal(profiles.private.length, 1, 'a private profile was made in the group')
  t.equal(profiles.public.length, 1, 'a public profile was made in the group')

  t.false(profiles.public[0].states[0].authors[member.ssb.id], 'member is not an author of the public profile yet')
  t.false(profiles.private[0].states[0].authors[member.ssb.id], 'member is not an author of the private profile yet')

  // ////
  // the kaitiaki adds the other member to the group
  // ////
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})

  // ////
  // the kaitiaki adds the member as a kaitiaki
  // ////
  const res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($groupId: String!, $adminIds: [String!]!) {
        addAdminsToGroup(groupId: $groupId, adminIds: $adminIds)
      }
    `,
    variables: {
      groupId,
      adminIds: [member.ssb.id]
    }
  })

  t.error(res.errors, 'adds member as kaitiaki')

  // ////
  // the member was added to the admin subgroup
  // ////
  const { adminSubGroupId } = await p(findAdminSubGroupByGroupId)(groupId)
  const admins = await p(kaitiaki.ssb.tribes.listAuthors)(adminSubGroupId)

  t.deepEqual(
    admins,
    [kaitiaki.ssb.id, member.ssb.id],
    'the new member is now a member of the admin subgroup!'
  )

  // ////
  // the member was added an author on the groups community profiles
  // ////
  profiles = await p(kaitiaki.ssb.profile.findByGroupId)(groupId)
  t.deepEqual(profiles.public[0].states[0].authors[member.ssb.id], [{ start: 0, end: null }], 'member is an author of the public profile!')
  t.deepEqual(profiles.private[0].states[0].authors[member.ssb.id], [{ start: 0, end: null }], 'member is an author of the private profile!')

  member.ssb.close()
  kaitiaki.ssb.close()
  t.end()
})
