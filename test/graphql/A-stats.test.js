const test = require('tape')
// const { promisify: p } = require('util')
const TestBot = require('../test-bot')

// function sleep (time) {
//   console.log(`sleeping: ${time}`)

//   return new Promise((resolve) => {
//     setTimeout(resolve, time)
//   })
// }

test('get latest message sequence', async t => {
  const { apollo, ssb } = await TestBot()

  const res = await apollo.query({
    query: `query {
      latestSequence
    }
    `
  })

  t.error(res.errors, 'Gets latest sequence without errors')
  t.equal(res.data.latestSequence, 9, 'Gets correct sequence number')

  ssb.close()
  t.end()
})

// SLOOOOW test (hyperlobs start/stop)
test('app storage information', async t => {
  const { ssb, apollo } = await TestBot({ hyperBlobs: { skip: false } })

  const res = await apollo.query({
    query: `
      query {
        ahauDBStats {
          size
          hyperBlobStats {
            size
          }
        }
        fileSystemStats {
          size
          used
          available
          use
          mount
        }
      }
    `
  })
  t.error(res.errors, 'gets ahau db stats without error')

  // NOTE: not currently testing what this returns

  ssb.close()
  t.end()
})

test('indexes stats', async t => {
  const { ssb, apollo } = await TestBot()

  async function getStats () {
    const res = await apollo.query({
      query: `
        query {
          indexes {
            isIndexing
            isRebuilding
            percentageIndexed
            percentageIndexedSinceStartup
          }
        }
      `
    })

    if (res.errors) throw res.errors

    return res.data.indexes
  }

  for (let i = 0; i < 100; i++) {
    ssb.publish({ type: 'counter', counter: i, recps: [ssb.id] }, (err) => { if (err) throw err })
  }

  let stats = await getStats()
  while (stats.isIndexing) {
    if (stats.percentageIndexed === 100) throw new Error('percentageIndexed wrong')
    if (stats.percentageIndexedSinceStartup === 100) throw new Error('percentageIndexedSinceStartup wrong')

    await new Promise(resolve => setTimeout(resolve, 50))
    stats = await getStats()
  }

  // console.log(stats)

  t.equal(stats.isIndexing, false, 'indexes.isIndexing')
  t.equal(stats.isRebuilding, false, 'indexes.isRebuilding')
  t.equal(stats.percentageIndexed, 100, 'indexes.percentageIndexed')
  t.equal(stats.percentageIndexedSinceStartup, 100, 'indexes.percentageIndexedSinceStartup')

  ssb.rebuild()
  stats = await getStats()
  t.equal(stats.isRebuilding, true, 'currently rebuilding')

  while (stats.isRebuilding) {
    await new Promise(resolve => setTimeout(resolve, 50))
    // console.log(stats)
    stats = await getStats()
  }
  t.equal(stats.isRebuilding, false, 'done rebuilding')

  ssb.close()
  t.end()
})
