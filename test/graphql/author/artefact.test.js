const test = require('tape')
const TestBot = require('../../test-bot')

const blobId = '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256'
const unbox = 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'

// graphql helper functions
function Save (apollo) {
  return async function save (input) {
    return apollo.mutate({
      mutation: `mutation($input: ArtefactInput) {
        saveArtefact(input: $input)
      }`,
      variables: { input }
    })
  }
}

function Get (apollo) {
  return async function get (id) {
    return apollo.query({
      query: `query {
        artefact(id: "${id}") {
          id
          canEdit
          tiaki {
            id
            preferredName
          }
          authors {
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
            }
          }
          type
          title
          description
          location
          blob {
            blobId
            mimeType
            size
            uri

            ...on BlobScuttlebutt {
              unbox
            }
            ...on BlobHyper {
              driveAddress
              readKey
            }
          }
          createdAt
          identifier
          licence
          rights
          source
          translation
          language
          recps
          ...on Video {
            duration
            transcription
          }
          ...on Audio {
            duration
            transcription
          }
        }
      }`
    })
  }
}

//
test('artefact - authors', async t => {
  // set the plan of number of test (so we know when done)
  t.plan(13)

  const { ssb, apollo } = await TestBot()

  const save = Save(apollo)
  const get = Get(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  // test 1
  // t.error expects undefined. or else it will fail
  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const artefactBlob = {
    type: 'ssb',
    blobId,
    mimeType: 'audio/mp3',
    size: 1212123,
    unbox
  }

  const saveArtefactRes = await save(
    {
      type: 'audio', // TODO: fails when '*' is used for the type
      authors: {
        add: [feedId],
        remove: []
      },
      blob: artefactBlob,
      recps: [feedId]
    }
  )
  // test 2 - does it save?
  t.error(saveArtefactRes.errors, 'creates artefact without errors')

  const artefactId = saveArtefactRes.data.saveArtefact

  // test 3 - basic get
  const artefactRes = await get(artefactId)
  t.error(artefactRes.errors, 'gets artefact without errors')

  // test 4
  // t.true bool test
  const artefact = artefactRes.data.artefact
  t.true(artefact.canEdit, 'returns true for canEdit')

  // test 5 - checking if you are creator/tiaki of this artefact
  t.deepEqual(artefact.tiaki, [{ id: publicProfileId, preferredName: null }], 'returns correct tiaki')

  // test 6 - tests the authors
  t.deepEqual(
    artefact.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors, remove feedId
  const saveRes = await save(
    {
      id: artefactId,
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      }
    }
  )
  t.error(saveRes.errors, 'updates artefact without errors')

  const updatedArtefactRes = await get(artefactId)
  t.error(updatedArtefactRes.errors, 'gets updated profile without error')

  const _artefact = updatedArtefactRes.data.artefact
  t.true(_artefact.canEdit, 'canEdit true still (because *)')

  // HACK: cant test dates?
  const start = _artefact.authors[1].intervals[0].start || 'SHOULD BE A TIME HERE!'

  t.deepEqual(
    _artefact.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId "again"
  await save(
    {
      id: artefactId,
      authors: {
        remove: [feedId] // the feedId should be ignored because its already removed
      }
    }
  )

  const deletedAuthorRes = await get(artefactId)
  t.error(deletedAuthorRes.errors, 'deletes authors without error')
  const artefact2 = deletedAuthorRes.data.artefact

  // HACK: cant test dates?
  const { end } = artefact2.authors[1].intervals[0]

  t.true(artefact2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    artefact2.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})
