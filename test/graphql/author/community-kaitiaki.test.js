const test = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')

const getProfile = `
query ($id: String!){
  profile(id: $id) {
    id
    preferredName
    recps
    ...on Community {
      kaitiaki {
        feedId
        profile {
          id
          email
          recps
        }
      }
    }
    authors {
      active
      feedId
      intervals {
        start
        end
      }
      profile {
        id
        legalName
        originalAuthor
        recps
      }
    }
  }
}
`

test('resolve community profile kaitiaki and authors', async t => {
  const { ssb: cherese, apollo } = await TestBot()

  const whoamiRes = await apollo.query({
    query: `
      query {  
        whoami {
          personal {
            profile {
              id
           }
          }
        }
      }
    `
  })

  t.error(whoamiRes.errors, 'query whoami')

  const whoami = whoamiRes.data.whoami

  async function savePerson (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($input: PersonProfileInput!) {
          savePerson (input: $input)
        }
      `,
      variables: {
        input
      }
    })

    t.error(res.errors, 'saves profile/person')

    return res.data.savePerson // profileId
  }

  // save some fields to our personal profile, so that the profile in the group has them
  await savePerson({
    id: whoami.personal.profile.id,
    preferredName: 'Cherese',
    legalName: 'Cherese Eriepa'
    // email: 'cherese@ahau.io' // TODO re-enable
  })
  console.log('TODO - re-enable saving email to personal profile')

  // create the group
  const communityProfile = {
    preferredName: 'Plant Collectors'
  }

  const groupRes = await apollo.mutate({
    mutation: `
      mutation ($communityProfile: CommunityProfileInput) {
        initGroup(communityProfile: $communityProfile) {
          groupId
        }
      }
    `,
    variables: {
      communityProfile
    }
  })

  const groupId = groupRes.data.initGroup.groupId

  const groupProfiles = await p(cherese.profile.findByGroupId)(groupId)

  // // now when we query the profile, we should see cherese profile on the authors and kaitiaki fields
  const communityRes = await apollo.query({
    query: getProfile,
    variables: {
      id: groupProfiles.private[0].key
    }
  })

  t.error(communityRes.errors, 'query community profile')

  const community = communityRes.data.profile

  cherese.profile.findByFeedId(cherese.id, (err, profiles) => {
    t.error(err, 'find chereses profiles')

    const profileInGroup = profiles.private.find(p => p.recps[0] === groupId)

    t.deepEqual(
      community,
      {
        id: groupProfiles.private[0].key,
        preferredName: 'Plant Collectors',
        recps: [groupId],
        kaitiaki: [
          {
            feedId: cherese.id,
            profile: {
              id: profileInGroup.key,
              email: null,
              recps: [groupId]
            }
          }
        ],
        authors: [
          {
            active: true,
            feedId: cherese.id,
            intervals: [{ start: 23, end: null }],
            profile: {
              id: profileInGroup.key,
              legalName: 'Cherese Eriepa',
              originalAuthor: cherese.id,
              recps: [groupId]
            }
          }
        ]
      },
      'returns kaitiaki + author profile as the one encrypted to the group'
    )

    cherese.close()
    t.end()
  })
})
