import AhauClient from 'ahau-graphql-client'
const SSBServer = require('scuttle-testbot')
const fetch = require('node-fetch')
const Application = require('ssb-tribes/method/deprecated/application.js')

const port = new Set([])
port.generate = function () {
  let port = 31000 + Math.random() * 10000 | 0
  while (this.has(port)) {
    port = 31000 + Math.random() * 10000 | 0
  }
  this.add(port)
  return port
}

module.exports = async function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,        (default: false)
  // }

  opts = {
    ...opts,
    db1: true,
    graphql: {
      port: port.generate(),
      skip: false,
      loadContext: true,
      ...opts.graphql
    },
    hyperBlobs: {
      skip: true,
      port: port.generate(),
      ...opts.hyperBlobs
    },
    migrate: {
      ...opts.migrate
    }
  }

  const stack = SSBServer // eslint-disable-line
    .use(require('ssb-conn'))

    /* @ssb-graphql/main deps */ // hmm may be out-dated sections
    .use(require('ssb-blobs'))

    /* @ssb-graphql/profile deps */
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-whakapapa'))
    .use(require('ssb-story'))
    .use(require('ssb-artefact'))
    .use(require('ssb-submissions'))
    .use(require('ssb-settings'))
    .use(require('ssb-invite'))
    .use(require('ssb-tribes'))
    .use(require('ssb-tribes-registration'))
    .use(require('ssb-lan'))

    .use(require('../')) // ssb-ahau!

  if (opts.hyperBlobs.skip === false) {
    stack.use(require('ssb-hyper-blobs')) // so it creates the hyperBlobs directory
  }

  if (opts.recpsGuard || opts.graphql.loadContext) {
    stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack(opts)

  if (opts.application === true) {
    ssb.tribes.application = Application(ssb)
  }

  // handleClose(ssb)
  await new Promise(resolve => ssb.ahau.onReady(resolve))

  if (opts.name) ssb.name = opts.name

  /* logging for debugging */
  if (opts.debug) {
    ssb.post(m => {
      if (m.value.author !== ssb.id) return

      ssb.get({ id: m.key, private: true }, (_, value) => {
        console.log(`\n${value.sequence} ${m.key}\n${JSON.stringify(value.content, null, 2)}`)
      })
    })
  }

  if (opts.graphql && opts.graphql.skip) return { ssb }

  const apolloClient = new AhauClient(`http://localhost:${opts.graphql.port}/graphql`, {
    isTesting: true,
    fetch
  })

  return {
    ssb,
    apollo: apolloClient,
    close: ssb.close
  }
}

/* a tool for debugging startup and shutdown */

// let instanceCount = 0
// const running = new Set([])

// function handleClose (ssb) {
//   const instance = ++instanceCount
//   console.log(instance, '↑ started')
//   running.add(instance)

//   let isRebuilding = false
//   ssb.rebuild.hook((rebuild, [cb]) => {
//     isRebuilding = true
//     rebuild(err => {
//       isRebuilding = false
//       cb && cb(err)
//     })
//   })
//   ssb.close.hook((close, [cb]) => {
//     if (isRebuilding) console.log('closing while still rebuilding!!!')
//     close(err => {
//       running.delete(instance)
//       if (err) console.log(instance, '✗ error in shutdown', err)
//       else console.log(instance, '↓ shutdown')

//       console.log('remaining to close:', Array.from(running))
//       cb && cb(err)
//     })
//   })
// }
