const test = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const Server = require('./test-bot')

const handleErr = (err) => {
  // console.log(JSON.stringify(err, null, 2))
  return { errors: [err] }
}

function InitGroup (apollo, t) {
  return async function (communityProfile) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($communityProfile: CommunityProfileInput) {
          initGroup(communityProfile: $communityProfile) {
            groupId
          }
        }
      `,
      variables: {
        communityProfile
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'init group')

    return res.data.initGroup.groupId
  }
}

test('multiple admin group kaitiaki (bug)', async t => {
  // ////
  // Setup users
  // ////
  const kaitiaki = await Server({ debug: !true })
  const secondKaitiaki = await Server({ debug: !true })
  const member = await Server()
  const name = id => {
    if (id === kaitiaki.ssb.id) return 'kaitiaki'
    if (id === secondKaitiaki.ssb.id) return 'secondKaitiaki'
    if (id === member.ssb.id) return 'member'
  }

  // ////
  // the kaitiaki initialises the group
  // ////
  const initGroup = InitGroup(kaitiaki.apollo, t)
  const communityProfile = {
    preferredName: 'Plant Collectors',
    description: 'For those who collect plants and want to share their collection',

    address: '123 Daisy Street',
    city: 'Hamilton',
    country: 'NZ',
    postCode: '3204',
    phone: '08 821 1128',
    email: 'plants@daisy.co.nz',

    joiningQuestions: [{ type: 'input', label: 'Do you collect plants?' }]
  }

  const groupId = await initGroup(communityProfile)

  // ////
  // the kaitiaki adds the the second kaitiaki to the group + as a kaitiaki
  // ////
  await p(kaitiaki.ssb.tribes.invite)(groupId, [secondKaitiaki.ssb.id], {})
  let res = await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($groupId: String!, $adminIds: [String!]!) {
        addAdminsToGroup(groupId: $groupId, adminIds: $adminIds)
      }
    `,
    variables: {
      groupId,
      adminIds: [secondKaitiaki.ssb.id]
    }
  })
    .catch(handleErr)
  t.error(res.errors, 'add admin to group')

  await replicate({ from: kaitiaki.ssb, to: secondKaitiaki.ssb, log: false })
  await replicate({ from: kaitiaki.ssb, to: member.ssb, log: false })

  // ////
  // the member applies to join the group
  // ////
  res = await member.apollo.mutate({
    mutation: `
      mutation(
        $groupId: String!
        $answers: [GroupApplicationAnswerInput]
        $comment: String
      ) {
        createGroupApplication(
          groupId: $groupId
          answers: $answers
          comment: $comment
        )
      }
    `,
    variables: {
      groupId,
      answers: [],
      comment: '',
      customFields: []
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'create application to the group')

  // ////
  // the application is sent to the both kaitiaki
  // ////
  await replicate({ from: member.ssb, to: kaitiaki.ssb, name, log: false })
  await replicate({ from: member.ssb, to: secondKaitiaki.ssb, name, lonlyog: false })
  await new Promise(resolve => setTimeout(resolve, 1000))

  // ////
  //  the second kaitiaki accepts the application
  // ////
  res = await secondKaitiaki.apollo.mutate({
    mutation: `
      mutation($id: String!, $comment: String, $groupIntro: String) {
        acceptGroupApplication(id: $id, comment: $comment, groupIntro: $groupIntro)
      }
    `,
    variables: {
      id: res.data.createGroupApplication,
      comment: '',
      groupIntro: ''
    }
  })
    .catch(handleErr)

  t.error(res.errors, 'the second kaitiaki can accept the application')

  // ////
  // the application is accepted by the second kaitiaki
  // ////
  // hypothesis - member has groupKey, so can decrypt messages
  //   - TRUE , they can decrypt new messages,
  //   - BUT they cannot decrypt all the old ones
  //

  await replicate({ from: secondKaitiaki.ssb, to: member.ssb, name, log: false })
  await replicate({ from: secondKaitiaki.ssb, to: kaitiaki.ssb, name, log: false })
  await new Promise(resolve => setTimeout(resolve, 1000))

  // ////
  //  the member is now apart of the group and can see the groups profiles
  // ////

  await checkPerspective(secondKaitiaki)
  await checkPerspective(kaitiaki)
  await checkPerspective(member)

  async function checkPerspective (bot) {
    const _name = name(bot.ssb.id)
    const members = await p(bot.ssb.tribes.listAuthors)(groupId)
    t.deepEqual(
      members.sort(),
      [kaitiaki.ssb.id, secondKaitiaki.ssb.id, member.ssb.id].sort(),
      _name + ': all three are members of the group'
    )

    res = await bot.apollo.query({
      query: `
        query {
          tribe(id: "${groupId}") {
            id
            public {
              id
            }
            private {
              id
            }
          }
        }
      `
    })
      .catch(handleErr)

    const profiles = await p(bot.ssb.profile.findByGroupId)(groupId)
    t.equal(profiles.private.length, 1, _name + ': sees private profile')
    t.equal(profiles.public.length, 1, _name + ': sees public profile')
  }

  member.ssb.close()
  kaitiaki.ssb.close()
  secondKaitiaki.ssb.close()

  t.end()
})
