const test = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const Server = require('./test-bot')

function sleep (time) {
  console.log(`sleeping: ${time}`)

  return new Promise((resolve) => {
    setTimeout(resolve, time)
  })
}

function waitTillIndexed (server) {
  return new Promise((resolve) => {
    const interval = setInterval(() => {
      if (server.status().sync.sync === true) {
        clearInterval(interval)
        resolve()
      } else console.log('indexing...')
    }, 300)
  })
}

// when do we need private profile created
// Profiles needed:
// - group profile
// - admin profile
// - links which you own!
//    - group profile (and admin profile) may have been linked by a kaitiaki
//
// NOT NEEDED:
// - personal groups - handled by @ssb-graphql/main loadContext
// - currently
//   - when you register with a group, if accepted, kaitiaki makes you a group profile
// - past
//   - if you used an application to join a group, then you will have made your own group-profile
//   - group creator running migration makes you an admin profile later + links it

test('auto-link-profiles (assert kaitiaki links)', async t => {
  t.plan(8)

  // start the server
  const kaitiaki = await Server({ name: 'kaitiaki', debug: false })
  const applicant = await Server({ name: 'applicant' })

  const name = id => id === kaitiaki.ssb.id ? 'kaitiaki' : 'applicant'

  const { groupId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })
  // console.log({ kaitiaki: kaitiaki.ssb.id, applicant: applicant.ssb.id, groupId })
  await p(kaitiaki.ssb.tribes.invite)(groupId, [applicant.ssb.id], {})

  let details = {
    preferredName: 'applicant',
    recps: [poBoxId, applicant.ssb.id],
    authors: { add: [applicant.ssb.id, '*'] }
  }
  const adminProfileId = await p(kaitiaki.ssb.profile.person.admin.create)(details)
  await p(kaitiaki.ssb.profile.link.create)(adminProfileId, { feedId: applicant.ssb.id })

  details = {
    preferredName: 'applicant',
    authors: { add: [applicant.ssb.id, kaitiaki.ssb.id] },
    recps: [groupId]
  }
  const groupProfileId = await p(kaitiaki.ssb.profile.person.group.create)(details)
  await p(kaitiaki.ssb.profile.link.create)(groupProfileId, { feedId: applicant.ssb.id })

  await replicate({ from: kaitiaki.ssb, to: applicant.ssb, name })

  await sleep(1000)
  await waitTillIndexed(applicant.ssb)

  async function checkProfileInGroup (groupId, profileId) {
    t.comment('> check profiles in group ' + groupId)
    const profiles = await p(applicant.ssb.profile.findByFeedId)(applicant.ssb.id, { groupId })
    const myGroupProfile = profiles.private[0]

    t.equal(myGroupProfile.key, profileId, 'same profile')

    t.equal(profiles.private.length, 1, 'I made a link to an existing profile in that group')
    t.equal(myGroupProfile.states[0].preferredName, 'applicant', 'preferredName correct')
    t.true(myGroupProfile.states[0].authors[applicant.ssb.id], 'applicant is author')
  }

  await checkProfileInGroup(groupId, groupProfileId)
  await checkProfileInGroup(poBoxId, adminProfileId)

  kaitiaki.ssb.close()
  applicant.ssb.close()
})
