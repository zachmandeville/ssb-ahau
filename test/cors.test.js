const test = require('tape')
const http = require('http')
const { promisify: p } = require('util')
const TestBot = require('./test-bot')

const PORT = 30600
const OK = 200
const BLOCKED = 500

function setENV (env) {
  for (const key in env) {
    process.env[key] = env[key]
  }
  console.info('set', env)
}

test('cors (development)', async t => {
  const initialENV = {
    NODE_ENV: process.env.NODE_ENV,
    AHAU_LOGGING: process.env.AHAU_LOGGING
  }
  setENV({
    NODE_ENV: 'development',
    AHAU_LOGGING: false
  })

  // start the server
  const { ssb } = await TestBot({
    graphql: {
      port: PORT
    }
  })

  await p(ssb.ahau.onReady)()
  let res

  res = await request({
    headers: {
      Origin: 'http://localhost:3000' // the location of ahau/desktop ui dev-server runs
    }
  })
  t.equal(res.statusCode, OK, '(in development) requests from ahau-desktop ui-dev-server allowed')

  res = await request({
    headers: {
      Origin: `http://localhost:${PORT}` // the location of ahau-desktop GraphiQL server
    }
  })
  t.equal(res.statusCode, OK, '(in development) GraphiQL still works')

  setENV(initialENV) // revert to what it was initially
  ssb.close()
  t.end()
})

test('cors (production)', async t => {
  const initialENV = {
    NODE_ENV: process.env.NODE_ENV,
    AHAU_LOGGING: process.env.AHAU_LOGGING
  }
  setENV({
    NODE_ENV: 'production', // NOTE undefined is also read as "production"
    AHAU_LOGGING: false
  })

  // start the server
  const { ssb } = await TestBot({
    graphql: {
      port: PORT
    }
  })

  await p(ssb.ahau.onReady)()
  let res

  res = await request({
    headers: {
      Origin: 'http://localhost:8080' // the location of ahau-desktop ui dev-server runs
    }
  })
  t.equal(res.statusCode, BLOCKED, 'requests from localhost blocked')

  res = await request({
    headers: {
      Origin: `http://localhost:${PORT}` // the location of ahau-desktop GraphiQL server
    }
  })
  t.equal(res.statusCode, OK, '(in production) GraphiQL still works')

  res = await request({
    headers: {
      // Origin: undefined // no Origin header set for request coming from Electron dom
    }
  })
  t.equal(res.statusCode, OK, 'requests from electron allowed')

  res = await request({
    headers: {
      Origin: 'http://pirate.com'
    }
  })
  t.equal(res.statusCode, BLOCKED, 'requests from http://pirate.com blocked')

  setENV(initialENV) // revert to what it was initially
  ssb.close()
  t.end()
})

function request (opts) {
  opts = {
    host: 'localhost',
    port: PORT,
    path: '/graphql',
    ...opts
  }
  return new Promise((resolve) => {
    const req = http.request(opts, (res) => resolve(res))
    req.end()
  })
}
