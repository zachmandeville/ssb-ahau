const test = require('tape')
const TestBot = require('./test-bot')

function setENV (env) {
  process.env.NODE_ENV = env
  // console.info('set NODE_ENV=', env)
}

test('init', t => {
  t.plan(1)

  TestBot()
    .then(({ ssb }) => {
      t.pass('started up')
      ssb.close()
    })
    .catch(err => t.error(err, 'init threw an error')) // should not be run
})

// this test passes in isolation, but fails in the suite... why?
// this only happens with tap-arc T_T
// tap-spec works fine
test('init (run migrations)', t => {
  t.plan(1)

  const initialENV = process.env.NODE_ENV
  setENV('production')

  TestBot()
    .then(({ ssb }) => {
      setENV(initialENV)
      t.pass('ran migrations, started up')
      ssb.close()
    })
    .catch(err => t.error(err, 'init (run migrations) threw an error')) // should not be run
})
