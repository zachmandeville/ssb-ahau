const test = require('tape')
const { promisify: p } = require('util')
const { replicate: _replicate } = require('scuttle-testbot')

const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1632944578931-add-person-to-admin-group')

test('migration/1632944578931-add-person-to-admin-group', async t => {
  const { ssb: kaitiaki } = await TestBot()
  const { ssb: applicant } = await TestBot()

  const name = (id) => id === kaitiaki.id ? 'kaitiaki' : 'applicant'
  const replicate = (from, to) => p(_replicate)({ from, to, name, log: !true })

  // ////////
  // Kaitiaki sets up a group
  // ///////

  // create a group
  const { groupId } = await p(kaitiaki.tribes.create)({})

  // create the admin subgroup for that group
  const { poBoxId } = await p(kaitiaki.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  let details = {
    preferredName: 'Alice',
    authors: {
      add: [kaitiaki.id]
    },
    recps: [groupId]
  }

  // the kaitiaki creates and links a profile for themselves in that group
  let profileId = await p(kaitiaki.profile.person.group.create)(details)
  await p(kaitiaki.profile.link.create)(profileId)

  details.recps = [poBoxId, kaitiaki.id]

  // and the admin subgroup
  profileId = await p(kaitiaki.profile.person.admin.create)(details)
  await p(kaitiaki.profile.link.create)(profileId)

  await replicate(kaitiaki, applicant)

  // ////////
  // Applicant applies to join and gets accepted!
  // ///////
  /* MIX - this is accurate to what happens but may not be necessary */

  // creates a registration
  // NOTE this is an old style application which doesn't create admin-profile
  const applicationId = await p(applicant.registration.tribe.create)(groupId, { recps: [poBoxId, applicant.id] })
  await replicate(applicant, kaitiaki)

  // the kaitiaki accepts it
  await p(kaitiaki.registration.tribe.accept)(applicationId, {})
  // the graphql accept also makes and links a group profile for applicant
  // NOTE this is the new way applications roll ...
  details = {
    preferredName: 'Bob',
    authors: {
      add: [applicant.id]
    },
    recps: [groupId]
  }
  const applicantGroupId = await p(kaitiaki.profile.person.group.create)(details)
  await p(kaitiaki.profile.link.create)(applicantGroupId, { feedId: applicant.id })

  // ////////
  // Run the migration!
  // ///////

  await p(migration.up)(kaitiaki, {})

  await replicate(kaitiaki, applicant)

  const profiles = await p(applicant.profile.findByFeedId)(applicant.id, { selfLinkOnly: false })
  // NOTE we need selfLinkOnly because the kaitiaki made the link!

  const adminProfile = profiles.other.private.find(profile => profile.recps[0].startsWith('ssb:identity/po-box'))
  t.deepEqual(adminProfile.recps, [poBoxId, applicant.id], 'admin.recps')
  t.deepEqual(adminProfile.states[0].preferredName, 'Bob', 'admin.preferredName')

  await Promise.all([
    p(kaitiaki.close)(true),
    p(applicant.close)(true)
  ])
  t.end()
})

/*
Considerations:
- only authored person profiles in the group
- dont include yours or others source profiles?
- IMPORTANT:
  - what if you are not the originalAuthor of a group and it has not been migrated yet to have a
  kaitiaki only subgroup, what do you do?
    - should this mean that only authors of the groups should be the one to migrate all profiles,
    even ones other people authored?
    - do you continue to check if there is a kaitiaki-only subgroup, and only when there is should you proceed to migrate
*/

// function wait (time) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => resolve(), time)
//   })
// }
