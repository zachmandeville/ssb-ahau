const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1659581313702-add-admins-to-groups')
const pull = require('pull-stream')

test('migration/1659581313702-add-admins-to-groups', async t => {
  const { ssb } = await TestBot({
    graphql: {
      loadContext: false
    }
  })

  let blockAdd = true
  ssb.publish.hook((publish, args) => {
    if (blockAdd) args[0].type = 'junk'
    publish(...args)
  })

  await p(ssb.tribes.create)({}) // old group (admin not added)
  blockAdd = false
  await p(ssb.tribes.create)({}) // new group

  // make sure there isnt a add admin to group msg
  function findAddAdmin () {
    return new Promise((resolve, reject) => {
      const query = [{
        $filter: {
          value: {
            content: {
              type: 'group/add-member'
            }
          }
        }
      }]
      pull(
        ssb.query.read({ query }),
        pull.filter(msg => msg.value.content.recps[1] === ssb.id),
        pull.collect((err, results) => {
          if (err) reject(err)
          else resolve(results)
        })
      )
    })
  }

  const results = await findAddAdmin()
  t.equals(results.length, 1, 'created new group and an old group (where an admin wasnt added)')

  await p(migration.up)(ssb, {})

  const newResults = await findAddAdmin()
  t.equals(newResults.length, 2, 'after migration admin was added to the group')

  await p(ssb.close)(true)
  t.end()
})
