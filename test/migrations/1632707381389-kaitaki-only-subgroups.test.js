const { promisify: p } = require('util')
const test = require('tape')
const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1632707381389-kaitaki-only-subgroups')

test('migration.kaitiaki-only-subgroups', async t => {
  t.plan(4)
  const { ssb } = await TestBot()

  async function hasAdminSubGroup (groupId) {
    const subgroups = await p(ssb.tribes.findSubGroupLinks)(groupId)
    const admin = subgroups.filter(link => link.admin)

    t.equal(admin.length, 1, 'has an admin subgroup!')
  }

  // create three groups
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: group2Id } = await p(ssb.tribes.create)({})
  const { groupId: group3Id } = await p(ssb.tribes.create)({})

  // add an admin subtribe to one
  await p(ssb.tribes.subtribe.create)(group2Id, { admin: true, addPOBox: true })

  // add a normal subtribe to another
  await p(ssb.tribes.subtribe.create)(group3Id, {})

  // checking the first one, it should have any subtribes
  const subgroups = await p(ssb.tribes.findSubGroupLinks)(groupId)
  const admin = subgroups.filter(link => link.admin)
  t.equal(admin.length, 0, 'has no admin subgroup yet!')

  // run the migration
  await p(migration.up)(ssb, {})

  // check it again, it should now have an admin subtribe
  await hasAdminSubGroup(groupId)
  await hasAdminSubGroup(group2Id)
  await hasAdminSubGroup(group3Id)

  await p(ssb.close)(true)
})
