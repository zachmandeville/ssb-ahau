const test = require('tape')
const { promisify: p } = require('util')
const LoadPersonalGroup = require('@ssb-graphql/main/src/ssb/load-context/load-personal-group')
const LoadContext = require('@ssb-graphql/main/src/ssb/load-context')

const SSB = require('../test-bot')
const migration = require('../../src/migrations/1632705677457-add-person-source-profile')

test('migration: personal profile type changed to profile/person/source', async (t) => {
  const { ssb } = await SSB({
    graphql: {
      loadContext: false, // don't load graphql context
      skip: true // don't start the graphql server
    },
    recpsGuard: true
  })

  const loadPersonalGroup = LoadPersonalGroup(ssb)
  const loadContext = LoadContext(ssb)

  /* Make an old style personal profile */
  const groupId = await loadPersonalGroup()
  const oldProfileId = await p(ssb.profile.person.group.create)({
    preferredName: 'manu',
    authors: { add: [ssb.id] },
    recps: [groupId]
  })
  t.ok(oldProfileId, 'create an old stype personal profile')
  await p(ssb.profile.link.create)(oldProfileId)
  t.pass('link old profile to myself')

  /* Migrate */
  await p(migration.up)(ssb, {})

  /* See if it works with loadContext */
  const context = await loadContext()
  const newProfile = await p(ssb.profile.person.source.get)(context.personal.profileId)

  for (const [key, value] of Object.entries(newProfile.states[0])) {
    if (key === 'preferredName') {
      t.equal(value, 'manu', 'fields copied over')
      continue
    }

    /* fields we expect to have things */
    if (key === 'key') continue
    if (key === 'type') continue
    if (key === 'authors') continue

    /* rest should be "empty" */
    if (value === null) continue
    if (Array.isArray(value) && value.length === 0) continue

    t.fail(`${key} should be empty, ${value}`)
  }

  await p(ssb.close)(true)
  t.end()
})
