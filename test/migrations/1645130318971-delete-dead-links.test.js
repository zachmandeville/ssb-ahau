const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1645130318971-delete-dead-links')

test('migration/1645130318971-delete-dead-links', async t => {
  const { ssb } = await TestBot({
    graphql: { skip: true },
    recpsGuard: true
  })
  // printMsgs(ssb)

  const { groupId } = await p(ssb.tribes.create)({})
  const recps = [groupId]

  // make profile
  const personId = await p(ssb.profile.person.group.create)({
    preferredName: 'Rhona',
    authors: { add: ['*'] },
    recps
  })
  const adminPersonId = await p(ssb.profile.person.admin.create)({
    preferredName: 'Rhona (admin)',
    authors: { add: ['*'] },
    recps
  })

  // attach links to it
  const A = '%TTAH8lW4fb1lQBJAtUPyT16OygdniSSlD5fRcatWv6A=.sha256'
  const B = '%S/2VP2qj6YIDuMf/JkVLRskP5ZbhiqDUsLSgMk42cjI=.sha256'
  // types from ssb-whakapapa (those which involve a profile)
  const linkInputs = [
    { type: 'link/profile-profile/child', parent: personId, child: A }, // tombstone (see below)
    { type: 'link/profile-profile/partner', parent: personId, child: A },
    { type: 'link/story-profile/contributor', parent: B, child: personId },
    { type: 'link/story-profile/creator', parent: B, child: personId, recps }, // recps
    { type: 'link/story-profile/mention', parent: B, child: adminPersonId } // adminPerson
  ]
  const linkIds = await Promise.all(
    linkInputs.map(input => {
      const details = input.recps ? { recps: input.recps } : { allowPublic: true }
      return p(ssb.whakapapa.link.create)(input, details)
    })
  )

  // tombstone one of the links
  // (to see if this messes up the migration)
  await p(ssb.whakapapa.link.tombstone)(linkIds[0], { allowPublic: true })

  // tombstone the person(s)
  await p(ssb.profile.person.group.tombstone)(personId, { reason: 'woops' })
  await p(ssb.profile.person.admin.tombstone)(adminPersonId, { reason: 'woops' })

  // run the migration
  const tombstoneCount = await p(migration.up)(ssb, {})

  // expect only 4 tombstones to be published (one link was already tombstoned)
  t.equal(tombstoneCount, 4, 'minimal number of required tombstoned')

  // check the links are now also tombstoned
  const links = await Promise.all(
    linkIds.map(linkId => p(ssb.whakapapa.link.get)(linkId))
  )

  links.forEach(link => {
    t.notEqual(link.states[0].tombstone, null, `tombstoned ${link.type}`)
  })

  await p(ssb.close)(true)
  t.end()
})

// function printMsgs (ssb) {
//   ssb.post(m => {
//     ssb.get({ id: m.key, private: true }, (_, val) => {
//       console.log(m.key)
//       console.log(JSON.stringify(val.content, null, 2))
//       console.log('-------')
//     })
//   })
// }
