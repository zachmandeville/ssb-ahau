const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1632802751433-add-pobox-to-community-profiles')

const emptyProfileFields = {
  tombstone: null,
  preferredName: null,
  description: null,
  address: null,
  city: null,
  country: null,
  postCode: null,
  phone: null,
  email: null,
  avatarImage: null,
  headerImage: null
}

test('migration/1632802751433-add-pobox-to-community-profiles', async t => {
  const { ssb } = await TestBot({ recpsGuard: true })

  // create an old style group which has a private and public profile attached to it
  const { groupId } = await p(ssb.tribes.create)({})

  // create a subgroup for that group
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  const authors = {
    add: [ssb.id]
  }
  // create and link a public community
  const publicProfileId = await p(ssb.profile.community.public.create)({ authors, allowPublic: true })
  await p(ssb.profile.link.create)(publicProfileId, { groupId, allowPublic: true })

  // create and link a private community
  const privateProfileId = await p(ssb.profile.community.group.create)({ authors, recps: [groupId] })
  await p(ssb.profile.link.create)(privateProfileId, { groupId })

  let groupProfiles = await p(ssb.profile.findByGroupId)(groupId)

  let expected = {
    public: [{
      key: publicProfileId,
      type: 'profile/community',
      originalAuthor: ssb.id,
      recps: null,
      states: [{
        type: 'community',
        key: publicProfileId,
        ...emptyProfileFields,
        joiningQuestions: [],
        customFields: {},
        poBoxId: null, // << not set
        authors: {
          [ssb.id]: [{ start: 15, end: null }]
        }
      }],
      ...emptyProfileFields,
      joiningQuestions: [],
      customFields: {},
      poBoxId: null,
      authors: {
        [ssb.id]: [{ start: 15, end: null }]
      },
      conflictFields: []
    }],
    private: [{
      key: privateProfileId,
      type: 'profile/community',
      originalAuthor: ssb.id,
      recps: [groupId],
      states: [{
        key: privateProfileId,
        type: 'community',
        ...emptyProfileFields,
        allowWhakapapaViews: null,
        allowPersonsList: null,
        allowStories: null,
        poBoxId: null, // << not set
        authors: {
          [ssb.id]: [{ start: 17, end: null }]
        }
      }],
      ...emptyProfileFields,
      allowWhakapapaViews: null,
      allowPersonsList: null,
      allowStories: null,
      poBoxId: null,
      authors: {
        [ssb.id]: [{ start: 17, end: null }]
      },
      conflictFields: []
    }]
  }

  t.deepEqual(
    groupProfiles,
    expected,
    'create and link group profiles'
  )

  // console.log('before', JSON.stringify(expected, null, 2))
  // console.log('after', JSON.stringify(groupProfiles, null, 2))

  console.time('1632802751433-add-pobox-to-community-profiles')
  const updatedProfileIds = await p(migration.up)(ssb, {})
  console.timeEnd('1632802751433-add-pobox-to-community-profiles')

  t.equal(updatedProfileIds.length, 2, 'two profiles were updated in the migration!')

  t.pass('run migration')

  groupProfiles = await p(ssb.profile.findByGroupId)(groupId)

  expected = {
    public: [{
      key: publicProfileId,
      type: 'profile/community',
      originalAuthor: ssb.id,
      recps: null,
      states: [{
        type: 'community',
        key: updatedProfileIds[0],
        ...emptyProfileFields,
        joiningQuestions: [],
        customFields: {},
        poBoxId,
        authors: {
          [ssb.id]: [{ start: 15, end: null }]
        }
      }],
      ...emptyProfileFields,
      joiningQuestions: [],
      customFields: {},
      poBoxId,
      authors: {
        [ssb.id]: [{ start: 15, end: null }]
      },
      conflictFields: []
    }],
    private: [{
      key: privateProfileId,
      type: 'profile/community',
      originalAuthor: ssb.id,
      recps: [groupId],
      states: [{
        type: 'community',
        key: updatedProfileIds[1],
        ...emptyProfileFields,
        allowWhakapapaViews: null,
        allowPersonsList: null,
        allowStories: null,
        poBoxId,
        authors: {
          [ssb.id]: [{ start: 17, end: null }]
        }
      }],
      ...emptyProfileFields,
      allowWhakapapaViews: null,
      allowPersonsList: null,
      allowStories: null,
      poBoxId,
      authors: {
        [ssb.id]: [{ start: 17, end: null }]
      },
      conflictFields: []
    }]
  }

  t.deepEqual(
    groupProfiles,
    expected,
    'group profiles have updated poBoxIds'
  )

  await p(ssb.close)(true)
  t.end()
})
