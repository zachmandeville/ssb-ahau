const test = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const TestBot = require('../test-bot')
const migration = require('../../src/migrations/1633378112058-move-applications-to-registrations')

async function setup (hasBox = true) {
  const applicant = await TestBot({ application: true })
  const kaitiaki = await TestBot({ application: true })

  // make a groupId with a kaitiaki
  const { groupId } = await p(kaitiaki.ssb.tribes.create)({})
  const { poBoxId } = await p(kaitiaki.ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

  const communityDetails = {
    poBoxId,
    authors: { add: [kaitiaki.ssb.id] },
    allowPublic: true
  }
  if (!hasBox) delete communityDetails.poBoxId
  const communityId = await p(kaitiaki.ssb.profile.community.public.create)(communityDetails)
  await p(kaitiaki.ssb.profile.link.create)(communityId, {
    groupId,
    allowPublic: true
  })

  const details = {
    answers: [{ q: "what's your fav colour?", a: 'luminescant orange' }],
    comment: 'hi!'
    // profileId: // no used by new registrations
  }
  const application = {
    open: await p(applicant.ssb.tribes.application.create)(groupId, [kaitiaki.ssb.id], details),
    accepted: await p(applicant.ssb.tribes.application.create)(groupId, [kaitiaki.ssb.id], details),
    rejected: await p(applicant.ssb.tribes.application.create)(groupId, [kaitiaki.ssb.id], details)
  }

  const name = (id) => id === kaitiaki.ssb.id ? 'kaitiaki' : 'applicant'
  await replicate({ from: applicant.ssb, to: kaitiaki.ssb, name, log: !true })

  await p(kaitiaki.ssb.tribes.application.accept)(application.accepted, {})
  await p(kaitiaki.ssb.tribes.application.reject)(application.rejected, {})

  await replicate({ from: kaitiaki.ssb, to: applicant.ssb, name, log: false })
  await new Promise(resolve => setTimeout(resolve, 100)) // sleep

  return { applicant, kaitiaki, name, groupId, poBoxId, application, details }
}

test('migration/1633378112058-move-applications-to-registrations', async t => {
  const { applicant, kaitiaki, groupId, poBoxId, application, details } = await setup(true)

  await p(migration.up)(applicant.ssb, {})

  // old application should be tombstoned
  const openApplication = await p(applicant.ssb.tribes.application.get)(application.open)
  t.notEqual(openApplication.tombstone, null, 'open application is tombstoned')

  const regs = await p(applicant.ssb.registration.tribe.list)({ accepted: null })
  t.equal(regs.length, 1, 'new registration made')
  t.equal(regs[0].groupId, groupId, 'reg.groupId correct')
  t.notEqual(regs[0].profileId, null, 'reg.profileId')
  t.deepEqual(regs[0].answers, details.answers, 'reg.answers correct')

  const historyComment = {
    type: 'comment',
    author: applicant.ssb.id,
    body: details.comment,
    timestamp: regs[0].history[1].timestamp // hack to be same
  }
  t.deepEqual(regs[0].history[1], historyComment, 'reg.answers correct')
  t.deepEqual(regs[0].recps, [poBoxId, applicant.ssb.id], 'reg.recps correct')

  // there should be a new application

  // NOTE - no idea why these awaits are needed, but seems to ensure tests pass T_T
  await Promise.all([
    p(kaitiaki.close)(true),
    p(applicant.close)(true)
  ])
  t.end()
})

test('migration/1633378112058-move-applications-to-registrations (no poBox)', async t => {
  // have discussed with Ben. He's happy for us to skip these cases where it's not possible
  // to migrate peoples applications
  const { applicant, kaitiaki, name, application } = await setup(false)

  await replicate({ from: kaitiaki.ssb, to: applicant.ssb, name, log: !true })
  await p(migration.up)(applicant.ssb, {})

  // old application should be tombstoned
  const openApplication = await p(applicant.ssb.tribes.application.get)(application.open)
  t.notEqual(openApplication.tombstone, null, 'open application is tombstoned')

  const regs = await p(applicant.ssb.registration.tribe.list)({ accepted: null })
  t.equal(regs.length, 0, 'no new registration made')

  // NOTE - no idea why these awaits are needed, but seems to ensure tests pass T_T
  await Promise.all([
    p(kaitiaki.close)(true),
    p(applicant.close)(true)
  ])
  t.end()
})
